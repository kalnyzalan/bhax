class Dog {
	public void makeSound() {
		System.out.println("Woof! Bark!");
	}
}

class GermanShepherd extends Dog {
	public void Guard () {}    //a német juhászok jó őrző védő kutyák
}

class Polimorph {
	public static void main(String[] args) {
		GermanShepherd german1 = new GermanShepherd();
		
		german1.makeSound();  //ezekkel nincsen baj, hiszen a "german1"
		german1.Guard();      //referencia Gyermekosztály típusú, tehát
		                      //megörökölte a makeSound() metódust is

		Dog german2 = new GermanShepherd();

		german2.makeSound();  //ezzel már baj van, mivel a "german2"
		//german2.Guard();      //Ősosztály típusú referencia, azaz csak 
		                      //a makeSound() metódust érjük el
	}
}


