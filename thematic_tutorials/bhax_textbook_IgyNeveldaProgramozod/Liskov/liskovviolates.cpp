#include <iostream>

class Vehicle {
	public: 
		void openDoor () {
			std::cout<<"Doors opened.\n";
		}
};

class Car : public Vehicle {
};

class Scooter : public Vehicle {
};

class Liskovviolates {
	public:
		void function(Vehicle& vehicle) {
			vehicle.openDoor();
		}
};

int main() {
	Liskovviolates program;
	Vehicle vehicle;
	program.function(vehicle);
	Car car;
	program.function(car);
	Scooter scooter;
	program.function(scooter);
}
