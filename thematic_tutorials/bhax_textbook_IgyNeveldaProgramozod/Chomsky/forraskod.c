#include <stdio.h>
#include <stdlib.h>

int f(int p, int q)
{
  if (p>q)
    return p;
  else return q;
}

int f(int p)
{
  return p*p;
}

int f(int* p)
{
  return (*p)*(*p);
}

int main()
{
  int i=0;
  int tomb[5];
  int n=10;
  int x=0;
  int y=1;
  int *d=&x;
  int *s=&y;
  int a=5;

  for(i=0;i<5;tomb[i]=i++)
	  printf("%d ",tomb[i]);
  printf("\n");

  for(i=0; i<n && (*d++ = *s++); ++i)
    printf("%d %d ",*d,*s);
  printf("\n");

  printf("%d %d", f(a, ++a), f(++a, a));
  printf("\n");

  printf("%d %d", f(a), a);
  printf("\n");

  printf("%d %d", f(&a), a);
  printf("\n");

  return 0;
}