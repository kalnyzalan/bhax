#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;
    printf("Az átváltandó decimális szám: ");
    scanf("%d",&n); //beolvassuk az átváltandó számot

    printf("Unáris számrendszerben: ");
    while (true)
    {
        if (n==0)
	    break;
	else
	    n-=1;
	    printf("|");
    }
    printf("\n");
}