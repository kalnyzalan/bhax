#include<stdio.h>
int main()
{
    //2 változó cseréje XOR operátorral
    int a=24, b=14;
    printf("Kezdeti értékek: a=%d, b=%d\n",a,b);
    a=a^b;
    b=a^b;
    a=a^b;
    printf("Felcserélt értékek: a=%d, b=%d\n",a,b);
    return 0;
}
