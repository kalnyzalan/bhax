#include <stdio.h>
int main()
{
    //2 változó cseréje összeadással és kivonással
    double a=-57.34, b=92.19;
    printf("Kezdeti értékek: a=%f, b=%f\n",a,b);
    b=b-a;
    a=b+a;
    b=a-b;
    printf("Felcserélt értékek: a=%f, b=%f\n",a,b);
    return 0;
}
