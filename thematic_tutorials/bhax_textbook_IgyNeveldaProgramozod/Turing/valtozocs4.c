#include <stdio.h>
int main()
{    
    //2 változó cseréje segédváltozóval
    double a=23.41, b=-46.2;
    double tmp; //temporary, azaz ideiglenes változó
    printf("Kezdeti értékek: a=%f, b=%f\n",a,b);
    tmp=a;
    a=b;
    b=tmp;
    printf("Kapott értékek: a=%f, b=%f\n",a,b);
}
