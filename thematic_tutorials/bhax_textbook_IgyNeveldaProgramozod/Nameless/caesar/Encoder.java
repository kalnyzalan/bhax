public interface Encoder {

	String encode(String text);
}
