import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		String fileName = args[0];
		int offset = Integer.parseInt(args[1]);
		try (StreamEncoder handler = new StreamEncoder(fileName, offset)) {
			handler.handleInputs();
		}
	}

}
