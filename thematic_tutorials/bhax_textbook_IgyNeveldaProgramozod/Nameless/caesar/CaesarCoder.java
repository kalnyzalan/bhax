import java.util.function.Function;

public class CaesarCoder implements Encoder {

	private final int offset;

	public CaesarCoder(int offset) {
		if (offset < 1 || offset > 26) {
			throw new IllegalArgumentException("Offset must be between 1 and 26");
		}
		this.offset = offset;
	}

	@Override
	public String encode(String text) {
		return buildString(text, character -> {
			if (character>='a' && character<='z')
				return (char) ((character - 'a' + offset) % 26 + 'a');
			else if (character>='A' && character<='Z')
				return (char) ((character - 'A' + offset) % 26 + 'A');
			else
				return character;
		});
	}

	private String buildString(String text, Function<Character, Character> function) {
		StringBuilder result = new StringBuilder();
		for (char character : text.toCharArray()) {
			if (character != ' ') {
				result.append(function.apply(character));
			} else {
				result.append(character);
			}
		}
		return result.toString();
	}

}
