import java.util.Random;
import java.lang.Math;

public class LZWfa {

    public class BinRandTree {

        protected class Node {

            private int value;
            private Node left;
            private Node right;

            public Node(int value) {
                this.value = value;
                left = null;
                right = null;
            }

            public int getValue() {
                return value;
            }

            public Node leftChild() {
                return left;
            }

            public Node rightChild() {
                return right;
            }

            public void leftChild(Node node) {
                left = node;
            }

            public void rightChild(Node node) {
                right = node;
            }
        }

        Node root = null;
        Node current = root;
        int depth = 0;
        int branchCount = 0;
        int sumBranchLength = 0;
        double meanBranchLength = 0;

        public void add(int value) {
            root = addMember(root, value);
        }

        public Node addMember(Node actual, int value) {
            Random rand = new Random();
            int randomNum = rand.nextInt(10) + 1;
            if (actual == null) {
                return new Node(value);
            } else if (actual.getValue() == value) {
                System.out.println("Már szerepel ilyen szám a fában.");
            } else if (randomNum == 1) {
                actual = root;
                this.addMember(actual, value);
            } else if (randomNum % 2 == 0) {
                actual.leftChild(addMember(actual.left, value));
            } else {
                actual.rightChild(addMember(actual.right, value));
            }
            return actual;
        }

        public void print() {
            printr(root);
        }

        public void printr(Node node) {
            if (node != null) {

                depth = depth + 1;
                printr(node.left);

                for (int i = 0; i < depth; i++)
                    System.out.print("---");
                System.out.print(node.getValue() + " " + depth + " \n");

                printr(node.right);
                depth = depth - 1;

            }
        }

        public int getDepth(Node node) {
            int b = 0, j = 0;
            if (node.leftChild() == null && node.rightChild() == null)
                return 1;
            if (node.leftChild() != null)
                b = getDepth(node.leftChild());
            if (node.rightChild() != null)
                j = getDepth(node.rightChild());
            depth = Math.max(b + 1, j + 1);
            return depth;
        }

        public int branchCount(Node node) {
            int count = 0;
            if (node.leftChild() == null && node.rightChild() == null)
                return 1;
            if (node.leftChild() != null)
                count = count + branchCount(node.leftChild());
            if (node.rightChild() != null)
                count = count + branchCount(node.rightChild());
            branchCount = count;
            return count;
        }

        public int sumBranchLength(Node node, int length) {
            int sum = 0;
            if (node.leftChild() == null && node.rightChild() == null)
                return length;
            if (node.leftChild() != null)
                sum = sum + sumBranchLength(node.leftChild(), length + 1);
            if (node.rightChild() != null)
                sum = sum + sumBranchLength(node.rightChild(), length + 1);
            sumBranchLength = sum;
            return sum;
        }

        public double meanBranchLength() {
            meanBranchLength = (double) sumBranchLength(root, 0) / branchCount(root);
            return meanBranchLength;
        }

        public double sumMeanDeviation(Node node, double meanBranchLength, int length) {
            double osszeg = 0;
            if (node.leftChild() == null && node.rightChild() == null)
                return (length - meanBranchLength) * (length - meanBranchLength);
            if (node.leftChild() != null)
                osszeg = osszeg + sumMeanDeviation(node.leftChild(), meanBranchLength, length + 1);
            if (node.rightChild() != null)
                osszeg = osszeg + sumMeanDeviation(node.rightChild(), meanBranchLength, length + 1);
            return osszeg;
        }

        public double deviation() {
            meanBranchLength = meanBranchLength();
            return Math.sqrt(sumMeanDeviation(root, meanBranchLength, 0) / (branchCount - 1));
        }
    }

    public class BinSearchTree extends BinRandTree {

        public Node addMember(Node actual, int value) {
            if (actual == null) {
                return new Node(value);
            } else if (actual.getValue() == value) {
                System.out.println("Már szerepel ilyen szám a fában.");
            } else if (actual.getValue() > value) {
                actual.leftChild(addMember(actual.leftChild(), value));
            } else {
                actual.rightChild(addMember(actual.rightChild(), value));
            }
            return actual;
        }
    }

    public class LZWTree extends BinRandTree {

        public LZWTree() {
            root = new Node(0);
            current = root;
        }

        public void add(int value) {
            if (value == 0) {
                if (current.leftChild() == null) {
                    current.leftChild(new Node(value));
                    current = root;
                } else {
                    current = current.leftChild();
                }
            } else {
                if (current.rightChild() == null) {
                    current.rightChild(new Node(value));
                    current = root;
                } else {
                    current = current.rightChild();
                }
            }
        }
    }

    public static void main(String[] args) {
        LZWfa tree = new LZWfa();
        LZWfa.LZWTree lzw = tree.new LZWTree();
        String input = "01111001001001000111";
        char[] toChar = input.toCharArray();
        int[] toInt = new int[toChar.length];
        for (int i = 0; i < toInt.length; i++) {
            toInt[i] = Character.getNumericValue(toChar[i]);
            lzw.add(toInt[i]);
        }
        lzw.print();
        System.out.println(lzw.getDepth(lzw.root) - 1);
        System.out.println(lzw.deviation());
    }

}