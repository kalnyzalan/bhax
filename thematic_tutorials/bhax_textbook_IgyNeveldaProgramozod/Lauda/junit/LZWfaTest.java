import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.util.Locale;

public class LZWfaTest {

    @Test
    public void testLZW() {
        // Given
        int expectedDepth = 4;
        double expectedDeviation = 0.957427;

        // When
        LZWfa actual = new LZWfa();
        LZWfa.LZWTree lzw = actual.new LZWTree();
        String input = "01111001001001000111";
        char[] toChar = input.toCharArray();
        int[] toInt = new int[toChar.length];
        for (int i = 0; i < toInt.length; i++) {
            toInt[i] = Character.getNumericValue(toChar[i]);
            lzw.add(toInt[i]);
        }
        int actualDepth = lzw.getDepth(lzw.root) - 1;
        double actualDeviation = lzw.deviation();
        String deviation = String.format(Locale.US, "%.6f", actualDeviation);
        actualDeviation = Double.parseDouble(deviation);

        // Then
        assertEquals(expectedDepth, actualDepth);
        assertEquals(expectedDeviation, actualDeviation);
    }
}
