import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.*;

/**
 * Small JSON serializer supporting strings, numbers, lists, booleans, null values and embedded objects.
 */
public class MiniObjectMapper {

    private static final Serializer<?> nullSerializer = new NullSerializer();
    private static final List<Serializer<?>> SERIALIZER_LIST = List.of(
            new StringSerializer(),
            new NumberSerializer(),
            new BooleanSerializer(),
            new ListSerializer(),
            new ObjectSerializer()
    );

    private final MainSerializer mainSerializer;

    /**
     * Creates a new MiniObjectMapper instance.
     */
    public MiniObjectMapper() {
        this.mainSerializer = new MainSerializer(SERIALIZER_LIST, nullSerializer);
    }

    /**
     * Creates a new MiniObjectMapper instance with the additional given serializers.
     *
     * @param customSerializerList custom serializer list to be used
     */
    public MiniObjectMapper(List<Serializer<?>> customSerializerList) {
        List<Serializer<?>> finalSerializerList = new ArrayList<>(customSerializerList);
        finalSerializerList.addAll(SERIALIZER_LIST);
        this.mainSerializer = new MainSerializer(finalSerializerList, nullSerializer);
    }

    /**
     * Returns the given object serialized into JSON.
     *
     * @param obj object to be serialized
     * @return given object in JSON serialized form
     */
    public String toJson(Object obj) {
        return mainSerializer.serialize(obj, mainSerializer);
    }

    private static ExampleData generateExampleData() {
        ExampleData.InnerData innerData = new ExampleData.InnerData("innerStringSerialized", 2131);
        return new ExampleData(
                "outerStringSerialized",
                1113,
                1134,
                1515L,
                3.141592654,
                Arrays.asList("first", "second", "third"),
                innerData,
                false);
    }

    public static void main(String[] args) {
        MiniObjectMapper test = new MiniObjectMapper();

        ExampleData v1 = generateExampleData();
        String v2 = "string";
        Long v3 = 12345L;
        List<ExampleData> v4 = List.of(generateExampleData(), generateExampleData());
        Boolean v5 = true;
        Object v6 = null;

        List<Object> list = Arrays.asList(v1, v2, v3, v4, v5, v6);
        list.forEach(v -> System.out.println(test.toJson(v) + "\n"));
    }

}

