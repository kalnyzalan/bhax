import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Solution {
    public void refactored() {
        Runnable runnable = () -> System.out.println("Runnable!");
        runnable.run();

        Calculator calculator = (number) -> number * number;
        System.out.println("Calculation result: " + calculator.calculate(3));

        List<Integer> inputNumbers = Arrays.asList(1, null, 3, null, 5);
        List<Integer> resultNumbers = new ArrayList<>();
        inputNumbers.forEach(number -> {
            if (number != null) {
                resultNumbers.add(calculator.calculate(number));
            }
        });

        Consumer<Integer> method = System.out::println;
        System.out.println("Result numbers: ");
        resultNumbers.forEach(method);

        Formatter formatter = (numbers) -> {
            StringBuilder sb = new StringBuilder();
            numbers.forEach(number -> {
                String stringValueOfNumber = String.valueOf(number);
                sb.append(stringValueOfNumber);
            });
            return sb.toString();
        };
        System.out.println("Formatted numbers: " + formatter.format(resultNumbers));
    }

    public static void main(String[] args) {
        new Solution().refactored();
    }
}
