<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Gutenberg!</title>
        <keywordset>
            <keyword/>
        </keywordset>
        <cover>
            <para>
                Programozás tankönyvek rövid olvasónaplói.
            </para>
        </cover>
    </info>
    <section>
        <title>Programozási alapfogalmak</title>
        <para>         
            Rövid olvasónapló a <citation>PICI</citation> könyvről.       
        </para>
	<para>
A programozási nyelveken belül három különböző szintet határozhatunk meg: vannak magas szintű nyelvek, assembly nyelvek, és gépi nyelvek. A könyv a magas szintű programozási nyelvekről ad ismereteket. Forráskódnak vagy forrásprogramnak nevezzük a magas szinten megírt kódokat, programokat.
	</para>
	<para>
Ezen forráskódokra vonatkozó formai megkötéseket, szabályokat nevezzük szintaktikának, míg szemantikának nevezzük nevezzük a jelentésbeli szabályokat.
	</para>
	<para>
Ahhoz, hogy az általunk megírt, ember által értelmezhető kódból a gép számára is értelmezhető kód legyen, szükség valamilyen fordítóprogramra, vagy interpreterre.
	</para>
	<para>
A fordító a következőképpen működik: a forráskódból object file-t (tárgykódot) hoz létre, eközben elemzi lexikálisan, azaz feldarabolja egységekre a kódot, majd elemzi szintaktikailag, azaz ellenőrzi a formai szabályokat, elemzi szemantikailag, majd ha ezek megtörténtek, kódot generál, ezzel kész a fordítás. Ahhoz, hogy futtatható legyen a programunk, további lépések szükségesek: a tárgykódból a linker (kapcsolatszerkesztő) hozza létre a futtatható programot, ezt a betöltő behelyezi az operatív tárba, így a program futásra készen áll. Futás közben a futtató rendszer felügyeli. Ezzel a módszerrel működik például a C, C++, Python, Java, stb. is.
	</para>
	<para>
Ezzel szemben az interpreteres megoldás abban különbözik, hogy nem készít object file-t/tárgykódot, hanem egyesével kéri be az utasításokat a felhasználótól, értelmezi és végre is hajtja őket egyből. Ilyen módszerrel működik például az R nyelv.
	</para>
	<para>
Hivatkozási nyelvnek nevezzük az egyes programozási nyelvek szabályrendszereit, azaz a szemantikai és szintaktikai szabályokat összefoglalva. Ezek mellett léteznek az implementációk, ezek a különböző fordítóprogramok és interpreterek.
	</para>
	<para>
A forráskódok legkisebb elemi egységei a karakterek. Minden forráskódhoz tartozik egy karakterkészlet, ami nyelvenként eltérő. Leggyakoribb típusai a betűk, számok, speciális karakterek. 
	</para>
	<para>
Lexikális egységeknek nevezzük a kód azon részeit, amiket önálló tokeneknek, a fordító által is felismert részeknek tekinthetünk. Ezek lehetnek: többkarakteres szimbólumok, szimbolikus nevek, címkék, megjegyzések, literálok. Többkarakteres szimbólumok pl. C++-ban ++, -- . Szimbolikus nevek: azonosítók: betűvel kezdődő, betűvel vagy számmal folytatódó karaktersorozatok (pl. C nyelven ezek a különböző objektumok nevei, mint pl. kutya); kulcsszavak: előre definált, adott nyelvre jellemző szavak, pl. if, else, while, stb. Címkék: pl. C nyelven, vagy Pythonban a goto kifejezés következtében a címkére ugrik a vezérlés, ott folytatódik a végrehajtása a programnak. Megjegyzések: olyan kódrészletek a forráskódban, amit a fordító nem értelmez, más embereknek szól, pl. segít a kódértelmezésben. Litálok: más néven konstansok, fix, állandó értékű adat, van értéke és típusa.  
	</para>
	<para>
C nyelven többféle típusát különböztetjük meg a konstansoknak; többek között vannak egész literálok (rövid (short) decimális, rövid oktális, rövid hexadecimális, előjel nélküli (unsigned), illetve hosszú (long)), vannak valós literálok, azok között is lehet rövid, hosszú, illetve kiterjesztett. Léteznek ezen kívül karakter és sztring konstansok is.
	</para>
	<para>
A programozási nyelvek kezdetben olyanok voltak, hogy a forráskód minden sorába csak egyetlen utasítást lehetett beleírni, mivel a sor vége egyúttal az utasítás végét is jelentette. Később azonban megjelent az elválasztójelek használata: leggyakrabban ez a pontosvessző ezekben a nyelvekben. Ez lehetővé tette, hogy egy sorba korlátlan mennyiségű utasítás kerüljön, valamint utasításokat akár több soron át is írhatunk. A szóköznek a legtöbb nyelvben kiemelt szerepe van a fordító számára, hiszen elhatárolójelként is szolgál. Ilyen elhatárolójel lehet még a vessző, kettőspont, stb.
	</para>
	<para>
A nyelveket aszerint is besorolhatjuk, hogy alkalmazza-e az adattípus fogalmát. A típust egyértelműen meghatározza a tartománya, amin az adott típusú adat/változó stb. felvehet értékeket; az ezen az adaton végezhető műveletek; illetve az adattípus ábrázolási módja (mennyi bájt szükséges a tárolásához). Sok nyelven, például C/C++-ban lehetőség van saját (user-defined) adattípus létrehozására (C++-ban ez a class).
	</para>
	<para>
A típusoknak több fajtáját különböztetjük meg: az első az egyszerű típusok, ilyenek például az egész típusok (lehet unsigned/előjel nélküli is), valós típusok, karakteres vagy sztring típusok, bool/logikai típusok. Megkülönböztetjük továbbá az összetett típusokat: ilyen a tömb, vagy a rekord. A tömbök kiemelt szerepűek, ugyanis gyakorlatilag az összes nyelv ismeri ezt. A tömbök elemei ugyanazon típus alá tartoznak, ugyanolyan műveleteket tudunk velük végezni, valamint lényeges tulajdonsága még, hogy fix méretű. A tömbök elemeit indexeléssel érjük el, C nyelven ez pl. szögletes zárójellel történik. Egyes nyelveken 0-tól, más nyelveken 1-től indul az indexelése. A harmadik típus a mutató típus, amely lényegében abban különbözik a többitől, hogy az ilyen típusú adatok tárcímeket (address-eket) tárolnak, és ezen cím révén el is tudják érni az adott címen tárolt adat értékét.
	</para>
	<para>
A legtöbb nyelvben léteznek nevesített konstansok. Ezeknek van nevük, típusuk, és értékük. C nyelven a #define kifejezéssel (ú.n. makróval) tudunk konstansokat deklarálni.
	</para>
	<para>
A változóknak 4 jellemzője van: van nevük, típusuk/attribútumuk, van értékük és van címük is. A legtöbb programnyelvben a deklarációnjuk explicit módon történik, ami azt jelenti, hogy a programozó végzi manuálisan. Változók kaphatnak tárhelyet statikusan: futtatáskor fix tárhelyet kapnak; kaphatnak dinamikusan: futás közben, ha aktivizálódnak egyes részek a kódban, kapnak tárhelyet; illetve a programozó is kioszthat manuálisan címet/tárhelyet neki. Változóknak az "=" operátorral adhatunk értéket.
	</para>
	<para>
C nyelven sokféle típust különböztetünk meg: vannak egészek, karakterek, van felsorolásos típus, vannak valósak, illetve tömbök, függvények, pointerek (mutatók), struktúrák, valamint ide soroljuk a void típust is.
	</para>
    </section>

    <section>
        <title>C programozás bevezetés</title>
        <para>                
            Rövid olvasónapló a <citation>KERNIGHANRITCHIE</citation> könyvről.
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/zmfT9miB-jY">https://youtu.be/zmfT9miB-jY</link>
        </para>
	<para>
Ez a könyv a C nyelv alapjait mutatja be; kezdőknek, illetve haladó/tapasztaltabb programozóknak egyaránt ajánlott. A könyv az első fejezetben bevezető dolgokkal, alapfogalmakkal indít, a legalapabb dolgokkal kezd: a "Hello mindenki!" egyszerű programot kezdi el magyarázni. Valószínűleg minden kezdő programozó, köztük én is ilyennel találkoztam először, mikor programozni kezdtem; a printf() (vagy C++-ban a cout) függvény segítségével kell kiíratni egy szöveges üzenetet a konzol képernyőjére. A könyv bemutatja azt is továbbá, hogy Unix alapú rendszeren hogyan lehet terminálból lefordítani a forráskódot, majd futtatni a programot.
	</para>	 
	<para>
A C nyelvű programok változókból és függvényekből állnak lényegében. A main() függvénynek minden programban benne kell lennie, hiszen itt kezdődik a program végrehajtása. Amikor meghívunk egy függvényt, argumentumokat adunk át nekik, azaz adatok egy listáját. Az adat átadása többféleképpen történhet, hiszen lehet érték szerint, illetve cím szerint (pointerekkel) átadni (illetve C++-ban referencia szerint is). A változók is a nyelv alapelemei, minden változót deklarálnunk kell azelőtt, mielőtt használnánk őket. A könyv továbbá egy példaprogramon keresztül beszél különböző adattípusokról, mint az int típus; a while ciklusról; ezek számomra már alapból is ismert dolgok voltak, szóval sok új dolgot nem tanultam belőlük. Ezután áttér a for ciklusra a könyv, ami szintén egy ismert dolog, a while ciklusnak egy alternatívája.
	</para>
	<para>
C-ben a #define kulcsszó segítségével deklarálhatunk/adhatunk meg konstansokat, vagy ahogy a könyv nevezi, szimbolikus állandókat. Ezt C++-ban továbbfejlesztették, ott a const szócskával deklarálhatunk adott típusú konstansokat.
	</para>
	<para>
Láthatunk példákat a tömbök használatára is a könyvben. A tömbök rögzített méretű adatstruktúrák, indexelésük 0-tól kezdődik C-ben. Beszél a könyv továbbá az elágazások (if-else-else if) használatáról, majd rátér a függvények részletezésére.
	</para>
    </section>   
     
    <section>
        <title>C++ programozás</title>
        <para>                
            Rövid olvasónapló a <citation>BMECPP</citation> könyvről.
        </para>
	<para>
Ebben a könyvben megismerkedhetünk a C++ nyelven való programozás sajátosságaival. A C++ lényegében a C nyelv továbbfejlesztése: minden, amit C nyelven le tudunk kódolni, az ugyanúgy működik C++-ban is, azonban számos újítást hoztak elődjével szemben.
	</para>
	<para>
Fontos kiemelni, hogy ezen újítások csak C++ nyelven értelmezhetőek, azaz a C fordítója már nem fogja tudni ezeket lefordítani. Jelentős változtatások történtek a függvények terén: míg C-ben egy függvénynek tetszőleges számú paramétere lehet, hogyha nem adunk meg egy argumentumot se, addig C++-ban ez lényegében olyan, mintha az argumentumok helyére egy voidot írnánk. A void szó űrt jelent, ezzel találkozhattunk C-ben is már, azonban ott csak a függvény visszatérési értékének a típusára használhattuk a kifejezést, C++-ban pedig argumentumként is megadhatjuk. Továbbá C-ben lehetőség van a visszatérési értéktípus elhagyására, hiszen az alapértelmezett típus int, míg C++-ban ez nem lehetséges.
	</para>
	<para>
Változtattak a main() függvényen is: egyrészt C++-ban lehetséges argumentumokat adni neki (int agrc--argumentumok száma, int argv[]--argumentumok), másrészt elhagyható lett a main() végéről a return kifejezés, ezzel automatikusan 0-t fog visszaadni értékként a program, amennyiben hiba nélkül lefutott.
	</para>
	<para>
Lényeges újítás volt a bool logikai típus/class bevezetése; bevezették a true=igaz és false, vagyis hamis értékeket. Így a változóknak adhatunk igaz-hamis értékeket is. A hamis értéknek a 0 felel meg, bármely más szám igazat jelöl. Ugyan C nyelven is létezett a logikai igaz-hamis, de ott egész számokkal reprezentálták azokat.
	</para>
	<para>
C++-ban alapértelmezetten tudja a fordító értelmezni a Unicode több bájtos karaktereket is, míg C-ben ehhez szükség volt egy külső könyvtár csatolásához.
	</para>
	<para>
C++-ban számos C-s könyvtári függvényen, kifejezésen fejlesztettek, illetve átláthatóbbá, megbízhatóbbá tették. Ezek körül kiemelendő például a struktúrák helyett a classok bevezetése, a malloc()/free() memórialefoglaló,-törlő függvény helyett a new és delete függvény bevezetése, a try-catch blokkok bevezetése, #define makrók helyett a konstansok bevezetése, pointerek helyett referencia szerinti értékátadás, illetve számos más újítás.
	</para>
    </section>    

    <section>
        <title>Python nyelvi bevezetés</title>
        <para>                
            Rövid olvasónapló a <citation>BMEPY</citation> könyvről.
        </para>
	<para>
Ez a könyv a mobilprogramozás sajátosságaiba ad betekintést, azonban én most csak a Python nyelvre vonatkozó bevezető részről fogok beszámolni. A Python egy objektumorientált, magas szintű programozási nyelv, hasonlóan például a C, vagy a C++ nyelvekhez. A Python továbbá egy szkriptnyelv, ami azt jelenti, hogy a kódunkat, magát a programot az értelmező a futtatás során értelmezi szemben más nyelvekkel, ahol fordítóprogramra van szükségünk, hogy futtatható állományt készítsünk a kódunkból. Ez nagyon sok esetben megkönnyíti a dolgunkat, hiszen nem kell minden egyes apró javítás esetén újrafordítanunk a kódot, hiszen a gép megteszi maga. A nyelv leginkább prototípus-programok tesztelésére, illetve egyszerűbb alkalmazások elkészítésére alkalmas.
	</para>
	<para>
Számos lényeges eltérés van más nyelvek, mint a C++ és a Python között. A kódban nincs szükség elválasztójelekre (pontosvessző, kapcsos zárójelek, stb..), helyette tabulátorozással, tördeléssel oldja meg a problémát. Továbbá a változóknak és függvényargumentumoknak nem kell megadni előre a típusát (pl. int, string, stb..), hiszen az értelmező automatikusan felismeri azt futtatáskor. Valamint egy másik szembetűnő különbség, hogy míg C-ben sokkal nagyobb munkával, kódolással tudjuk elérni a célunkat, addig Pythonban bonyolultabb kifejezéseket tudunk leírni rövidebben, tömörebben.
	</para>
	<para>
Ahogy az előbb említettem, Pythonban tabulátorokkal, illetve spacekkel tagoljuk a kódunkat. Az azonos behúzású sorok egy logikai egységet alkotnak.
	</para>
	<para>
A kódban minden sort úgynevezett tokenekre bont az értelmező, ezek a tokenek a különféle megadható karakterek, szavak, stb... amelyeket felhasználhatunk. Különböző típusai: azonosítók (változók, függvények nevei, stb..), kulcsszavak (pl. if, else, elif, and, or, stb..), delimiterek/elválasztójelek (pl. vessző, kettőspont), literálok (állandók/konstansok) és az operátorok, mint például az összeadás- vagy kivonásjel ("+","-").
	</para>
	<para>
A C-hez és C++-hoz hasonlóan a Python is objektumorientált nyelv, és az objektumoknak/adatoknak különféle típusaik lehetnek, ezek a következők: számok (egész, illetve lebegőpontos és komplex számok), sztringek (az 'u' előtag hozzátevésével Unicode kódolású sztringeket adhatunk meg), ezeket idézőjelek közé kell tennünk. Felvehetünk adat n-eseket is (angolul tuples), illetve listákat is, a különbség a kettő között csupán annyi, hogy az utóbbi mérete módosítható, azaz tudunk felvenni a listába további adatokat. Ezen kívül megadhatunk szótárakat is, ez lényegében adatpárokból épül fel, minden eleméhez tartozik egy azonosító vagy kulcs, amely egyértelműen meghatározza azt.
	</para>
	<para>
Ahogy korábban említettem már, a változóknak nincsen típusa a kódban Pythonban, így a felhasználásuktól függően különböző típusú adatokra is vonatkozhatnak futtatás során akár. Egy sorban egyszerre több változónak is adhatunk értéket (azonos értéket), létrehozhazunk sorozatokat is, valamint akár 2 változó értékét is felcserélhetjük az a,b=b,a egyszerű kifejezéssel, ezzel szemben C nyelven például azt nem tudjuk ilyen könnyen megtenni. Változókat a global kulcsszóval tehetünk globálissá.
	</para>
	<para>
A különböző típusú számok, pl. int-float között létezik konverzió. Sztringeken, listákon, szekvenciákon különböző műveleteket végezhetünk, összefűzhetjük őket (a + operátorral), lekérhetjük a hosszukat (a len függvénnyel), stb. Egy szekvent adott indexű elemét szögletes zárójellel érjuk el, pl. a[2] az a szekvent 3. eleme, láthatjuk, hogy az indexelés itt is 0-tól indul. Listákon végezhetünk különféle műveleteket függvények segítségével, például lekérhetjük egy adott indexű elemét, sorbarendezhetjük, kitörölhetünk vagy hozzáadhatunk elemet, stb. Hasonlóan szótárakon is értelmezve vannak különféle függvények, például elemeket törölhetünk, megkereshetünk benne egy adott kulcsot, lekérhetjük a különböző adatokat, kulcsokat, adat-kulcspárokat.
	</para>
	<para>
A nyelv egyik legalapvetőbb függvénye a print() függvény, amely értelemszerűen a standard outputra, konzolra ír ki.
	</para>
	<para>
A többi nyelven hasonló módon működnek az elágazások, azaz az if, elif (vagyis az else if) és az else kifejezések, amikről nem is nagyon kell beszélni sokat.
	</para>
	<para>
A ciklusok, azaz a while, és a for ciklus is hasonlóan működnek, kis kiegészítéssel. For esetében használhatjuk az in kulcsszót, és a range() függvényt, amely megmondja, hogy a ciklusváltozó milyen intervallumban mozogjon. A gyakorlatban: pl. for i in range(0,10). A while esetében a feltétel körül elhagyható a zárójel, és a végére kettőspontot kell tennünk.
	</para>
	<para>
Egyedi megoldás a címkék (labelek) használata. A goto 'címke' kulcsszó megadásával a kód futásakor az értelmező a címke sorához ugrik, és onnan folytatódik a végrehajtás. A comefrom 'címke' kifejezéssel pedig az ellenkezőjét érjük el: az előtte lévő címkéről a comefrom sorára ugrik, majd innen folytatódik tovább.
	</para>
	<para>
A függvények esetében a megszokott dolgokon kívül csak néhány változtatás van: például csak érték szerint adhatunk át paramétert, kivéve pl. lista, szótár esetében, ott a hívott függvény megváltoztathatja a hívó függvény által átadott paramétereket. A def kulcsszóval definiáljuk őket.
	</para>
	<para>
Az osztályok (classok) esetében is hasonló a helyzet, mint sok más nyelvnél: definiálunk egy osztályt, amelynek lehetnek tagfüggvényei, illetve tagobjektumai (változók). A függvényeinek kötelezően meg kell adni egy 'self' nevű első paramétert, ez azt az objektumot adja meg, amellyel meghívták a függvényt.
	</para>
	<para>
A C++-ból is ismert try-catch-es kivételkezeléshez hasonlóan a Python is támogatja a kivételkezelést. A try kulcsszó után áll a vizsgálandó kódrészünk, amely lefutásában keressük a hibát. A try blokk után áll az except blokk: ez a rész akkor fut le, amikor a try blokkban előáll a kivételes esetünk. További, nem kötelező rész az ezután álló else ág.
	</para>
	<para>
Lehetőség van Pythonban továbbá modulok készítésére, felhasználására, importálására. A modulok olyan kódrészletek, fájlok, amelyek felhasználó által készített függvényeket, eljárásokat, konstansokat, stb. tartalmaznak, és ezek felhasználhatók más Python programokban. Egy programban az import szóval tudunk modulokat importálni, pl.: from 'modulnév' import 'függvény'.
	</para>
    </section>
</chapter>                
